/* eslint-disable @typescript-eslint/explicit-function-return-type */

exports.up = (knex) => {
  return knex.schema.createTable('projects', table => {
    table.increments('id').primary()
    table.string('name').notNullable()
    table.string('description').notNullable()
    table.string('url').notNullable()
    table.string('slug').notNullable()
    table.integer('owner_id').unsigned()
    table.foreign('owner_id').references('id').inTable('authors')
    table.unique('url')
    table.index('url')
  })
}

exports.down = (knex) => {
  return knex.schema.dropTable('projects')
}
