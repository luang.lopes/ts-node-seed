/* eslint-disable @typescript-eslint/explicit-function-return-type */

exports.up = (knex) => {
  return knex.schema.createTable('projects', table => {
    table.increments('id').primary()
    table.string('name').notNullable()
    table.string('avatar_url').nullable()
    table.string('username').notNullable()
    table.unique('username')
    table.index('username')
  })
}

exports.down = (knex) => {
  return knex.schema.dropTable('projects')
}
