/* eslint-disable @typescript-eslint/no-var-requires */
require('dotenv').config()

const databaseConfig = {
  host: process.env.DB_HOST || '127.0.0.1',
  port: process.env.DB_PORT || 3306,
  username: process.env.DB_USER || 'root',
  password: process.env.DB_PASS || '',
  database: process.env.DB_NAME || 'ts-node-seed',
  dialect: process.env.DB_DIALECT || 'mysql2',
}

module.exports = {

  development: {
    client: databaseConfig.dialect,
    connection: {
      host: databaseConfig.host,
      user: databaseConfig.username,
      password: databaseConfig.password,
      database: databaseConfig.database,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },

  staging: {
    client: databaseConfig.dialect,
    connection: {
      host: databaseConfig.host,
      user: databaseConfig.username,
      password: databaseConfig.password,
      database: databaseConfig.database,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },

  production: {
    client: databaseConfig.dialect,
    connection: {
      host: databaseConfig.host,
      user: databaseConfig.username,
      password: databaseConfig.password,
      database: databaseConfig.database,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
    },
  },

}
