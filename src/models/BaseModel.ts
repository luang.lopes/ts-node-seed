import objection from 'objection'
import visibilityPlugin from 'objection-visibility'
import { DBErrors } from 'objection-db-errors'

export class BaseModel extends visibilityPlugin(DBErrors(objection.Model)) { }
