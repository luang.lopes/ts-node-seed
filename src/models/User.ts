import visibilityPlugin from 'objection-visibility'
import bcrypt from 'bcrypt'

import { BaseModel } from '@app/models/BaseModel'

export class User extends visibilityPlugin(BaseModel) {
  readonly id!: number
  name?: string
  email?: string
  password?: string

  static tableName = 'persons'

  static hidden = ['password']

  static jsonSchema = {
    type: 'object',
    required: ['name', 'email'],
    properties: {
      id: { type: 'integer' },
      name: { type: 'string', minLength: 3, maxLength: 255 },
      email: { type: 'string', minLength: 5, maxLength: 255 },
      password: { type: 'string', minLength: 6, maxLength: 255 },
    },
  }

  async $beforeSave (): Promise<void> {
    if (this.password) {
      this.password = await bcrypt.hash(this.password, await bcrypt.genSalt(12))
    }
  }

  async $beforeInsert (): Promise<void> {
    await this.$beforeSave()
  }

  async $beforeUpdate (): Promise<void> {
    await this.$beforeSave()
  }

  async verifyPassword (password: string): Promise<boolean> {
    const isRightPassword = !!this.password && await bcrypt.compare(password, this.password)

    return isRightPassword
  }
}
