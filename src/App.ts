import express, { Application, ErrorRequestHandler } from 'express'
import cors from 'cors'
import morgan from 'morgan'
import helmet from 'helmet'
import { Model, knexSnakeCaseMappers } from 'objection'
import Knex from 'knex'
import createHttpError from 'http-errors'

import { envConfig, databaseConfig } from '@app/config'

class App {
  public express: Application;

  public constructor () {
    this.express = express()

    this.databese()
    this.middlewares()
    this.routes()
  }

  private async databese (): Promise<void> {
    try {
      const knex = Knex({
        client: databaseConfig.dialect,
        connection: {
          host: databaseConfig.host,
          user: databaseConfig.username,
          password: databaseConfig.password,
          database: databaseConfig.database,
        },
        ...knexSnakeCaseMappers(),
      })

      Model.knex(knex)

      console.info('Databse connected')
    } catch (error) {
      console.error('Error connecting database', error)

      process.exit(1)
    }
  }

  private middlewares (): void {
    this.express.use(helmet())
    this.express.use(morgan('dev'))
    this.express.use(express.json())
    this.express.use(express.urlencoded({ extended: false }))
    this.express.use(cors)
  }

  private routes (): void {
    // handle 404
    this.express.use((req, res, next) => {
      next(createHttpError(404))
    })
    // register default error hanlder
    this.express.use(this.errorHandler())
  }

  private errorHandler (): ErrorRequestHandler {
    return (error, _req, res): void => {
      const statusCode = error.status || 500
      let errorMessage = error.message || 'Internal server error'

      if (statusCode === 500 && envConfig.isProduction) {
        // Don't return unhandled error messages
        errorMessage = 'Internal server error'
      }

      if (envConfig.isDevelopment) {
        // log complete error stack
        console.error(error)
      } else {
        // log only error message
        console.error(error.message)
      }

      res.status(statusCode).json({ ...error, message: errorMessage })
    }
  }
}

export default new App().express
