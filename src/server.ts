import dotenv from 'dotenv'

dotenv.config()

/* eslint-disable import/first */
import http from 'http'

import App from '@app/App'
import { httpConfig } from '@app/config'

const server = http.createServer(App)

server.listen(httpConfig.port)

server.on('listening', () => {
  console.log(`Server linstening on port ${httpConfig.port}`)
})
